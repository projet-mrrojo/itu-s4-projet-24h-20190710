<?php
session_start();
    class montantController extends CI_Controller {

        function __construct() {
            parent::__construct();
            $this->load->helper('url');
            $this->load->database();
        }

        public function index(){
            $this->load->helper('form');
            $data['page']="getMontant";
            $sql="select * from tab";
            $data['result']=$this->db->query($sql)->result();
            $this->load->view('page',$data);
        }

        public function get(){
            $this->load->helper('form');
            $id = $this->input->post('table');
            $this->load->model('Commandes');
            $res = $this->Commandes->getMontant($id);
            $montant = 0;
            for($i=0;$i<count($res[0]);$i++){
                $montant += $res[0][$i]->prix;

            }
            $data['montant'] = $montant;
            $data['plats'] = $res[0];
            $data['idCommande'] = $res[1];
            $data['page']="insertPayement";
            $this->load->view('page',$data);
        }

    }
?>