<?php
session_start();
    class indexController extends CI_Controller{

        function __construct() { 
            parent::__construct(); 
            $this->load->helper('url'); 
            $this->load->database(); 
            $this->load->model('Serveur');
            $this->load->model('Plat');
            $this->load->model('Parametre');
         } 
        
        public function index(){
            $this->load->helper('form'); 
            $this->load->helper('url');
            $this->load->view('login');
        }

        public function Menu(){
            $data['list'] = $this->Plat->getToday();
            $data['page']='home';
            $this->load->helper('url');
            $data['param']=$this->Parametre->getParametre();
            $this->load->view('page',$data);
        }

        public function check(){
            $login = $this->input->post('login');
            $password = $this->input->post('password');   
            $this->db->select('id,nom,motDePasse,profil');                     
            $this->db->from('serveur');                     
            $this->db->where('nom =',$login);                     
            $this->db->where('motDePasse =',$password);                     
            $query = $this->db->get()->row();
            if(count($query)>0){
                if($query->profil==1){ 
                    $_SESSION['serveur']=$query;                     
                    $this->Menu();
                    
                }
                if($query->profil==0){
                    $this->load->helper('form');
                    $_SESSION['serveur']=$query;
                    $data['page']="accueil";
                    $this->load->view('pageAdmin',$data);
                }
            }
            else{
                redirect('indexController');
            }            
            
        }

        public function plus($id){
            if(!isset($_SESSION['commande'])){
                $_SESSION['commande']=array();
            }
            $commande=$_SESSION['commande'];
            $v=0;
            for($i=0;$i<count($commande);$i++){
                if($id==$commande[$i]['id']){
                    $commande[$i]['nombre']++;
                    $v=1;
                    break;
                }
            }
            if($v==0){
                $commande[]=array('id'=>$id,'nombre'=>1);
            }
            $_SESSION['commande']=$commande;
            $this->Menu();
        }

        public function Deconnexion(){
            session_destroy();           
            $this->index();
        }

    }
?>