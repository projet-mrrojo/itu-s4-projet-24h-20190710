<?php
session_start();
    class platController extends CI_Controller{

        function __construct(){
            parent::__construct();
            $this->load->helper('url');           
        }

        public function index(){
            $this->load->model('Plat');
            $query=$this->Plat->getPlat();
            $data['plat']=$query->result();
            $data['page']="listPlat";
            $this->load->view('page', $data);
        }

    }
?>