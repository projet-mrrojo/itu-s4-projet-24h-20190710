<?php
session_start();
    class accueilController extends CI_Controller{

        function __construct(){
            parent::__construct();
            $this->load->helper('form');
            $this->load->model('Plat');
            $this->load->helper('url');
        }

        public function creer(){
            $data['page']="creer";
            $this->load->view('pageAdmin',$data);
        }

        public function insert(){
            $this->load->database();
            $this->Plat->insert($this->input->post('nom'),$this->input->post('categorie'),$this->input->post('prix'));
        }

        public function lister(){        
            $query=$this->Plat->getPlat();
            $data['plat']=$query->result();
            $data['page']="lister";
            $this->load->view('pageAdmin', $data);
        }
        
        public function changer(){
            $data['id']=$this->input->post('id');
            $data['page']="update";
            $this->load->view('pageAdmin',$data);
        }

        public function update(){
            $nom=$this->input->post('nom');
            $prix=$this->input->post('prix');
            $cat=$this->input->post('categorie');
            $id=$this->input->post('id');
            $this->Plat->update($id,$nom,$cat,$prix);
            $this->lister();
        }

        public function delete(){
            $id=$this->input->post('id');
            $this->Plat->delete($id);
            $this->lister();
        }

        public function parametre(){
            $this->load->model('Parametre');
            $data['param']=$this->Parametre->getParametre();
            $data['page']="parametre";
            $this->load->view('pageAdmin',$data);
        }

        public function parametrer(){
            $this->load->model('Parametre');
            $param=$this->Parametre->getParametre();
            for($i=0;$i<count($param);$i++){
                $param[$i]->valeur=$this->input->post($param[$i]->designation);
            }
            $this->Parametre->parametrer($param);
            $data['page']="accueil";
            $this->load->view('pageAdmin',$data);
        }

        public function recherche(){
            $this->load->model('Search');
            $this->load->helper('form');
            $string = $this->input->post('search');
            $string1 = $this->input->post('categorie');
            if($string1==""){
                $list = $this->Search->searchPlat($string);
                $data['list'] = $list;
                $data['page']="search";
                $this->load->view('pageAdmin',$data);    
            }
            else{
                $list = $this->Search->searchPlatCat($string,$string1);
                $data['list'] = $list;
                $data['page']="search";
                $this->load->view('pageAdmin',$data);
            }            
        }

        public function today(){
            $nb=$this->input->post('nb');
            $this->load->database();
            $ids=array();           
            for($i=0,$v=0;$i<$nb;$i++){                
                if($this->input->post($i+1)){
                    $ids[$v]=$this->input->post($i+1);
                    $v++;
                }
            }
            $sql="update today set idplats='";
            for($i=0;$i<count($ids);$i++){
                $sql=$sql."".$ids[$i].",";
            }
            $sql=substr($sql,0,-1);
            $sql=$sql."'";
            $this->db->query($sql);
            $this->load->helper('url');
            $data['page']="accueil";
            $this->load->view('page',$data);
        }
    }

?>