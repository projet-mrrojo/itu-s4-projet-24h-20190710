<section class="ftco-section">   
   <div class="container">
    	<div class="row justify-content-center mb-5 pb-3 mt-5 pt-5">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <h2 class="mb-4">Montant A Payer:</h2>                
                <div class="col-md-12 heading-section text-center d-flex justify-content-around">
         <?php
            echo form_open('PayementController/pay',array('class' => 'text-center border border-dark p-5'));            
            echo form_input(array('name'=>'idCommandes','value'=>$idCommande,'type'=>'hidden'));
            echo "<h5>".form_label('Montant :')."</h5>";
            echo "<p>".form_input(array('name'=>'montant'))."</p>";            
            echo "<p>".form_submit(array('value'=>'Payer','class' => 'btn btn-info btn-block my-4'))."</p>";
            echo form_close();
         ?>
</div>
                               
            </div>
        <div class="row">  
            <div class="col-md-12"> 
            <?php 
                $i=1;
                foreach($plats as $plat){            
            ?>
                <div class="pricing-entry d-flex ftco-animate">
        			<div class="img" style="background-image: url(images/pizza-5.jpg);"></div>
        			   <div class="desc pl-3">
	        			    <div class="d-flex text align-items-center">
	        				   <h3><span><?php echo $plat->nom; ?></span></h3>
                                <span class="price"><?php echo $plat->prix; ?></span>                          
	        			    </div>	        			   
	        		   </div>
        		    </div>
            <?php  
                 $i = $i+1;                                                            
            }
            echo "<h3>Total=       ".$montant." Ar</h3>";    
            ?>
         </div>
      </div>
   </div>
</section>

