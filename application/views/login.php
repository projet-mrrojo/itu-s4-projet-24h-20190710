<!DOCTYPE html> 
<html lang = "en">
 
   <head> 
      <meta charset = "utf-8"> 
      <title>Login</title> 
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="<?php print(base_url());?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php print(base_url());?>assets/css/mdb.min.css" rel="stylesheet">
      <link href="<?php print(base_url());?>assets/css/mdb.css" rel="stylesheet">
      <link href="<?php print(base_url());?>assets/css/bootstrap.css" rel="stylesheet">
      <link href="<?php print(base_url());?>assets/css/mdb.lite.css" rel="stylesheet">
      <link href="<?php print(base_url());?>assets/css/mdb.lite.min.css" rel="stylesheet">
      <link href="<?php print(base_url());?>assets/css/style.css" rel="stylesheet">
      <link href="<?php print(base_url());?>assets/css/style.min.css" rel="stylesheet">
   </head> 
   <body>
      <div class="container"> 
         <div class="d-flex justify-content-around">
         <?php 
            echo form_open('indexController/check',array('class' => 'text-center border border-dark p-5'));
            echo form_label('Login',array('class' => 'h4 mb-4 text-dark')); 
            echo "<br/>";
            echo form_input(array('id'=>'login','name'=>'login','class' => 'form-control mb-4','placeholder' => 'User')); 
            echo "<br/>"; 
			
            echo form_label('Password',array('class' => 'h4 mb-4 text-dark')); 
            echo "<br/>";
            echo form_input(array('id'=>'password','name'=>'password','type'=>'password','class' => 'form-control mb-4','placeholder' => 'password')); 
            echo "<br/>"; 
			
            echo form_submit(array('id'=>'submit','value'=>'Login','class' => 'btn btn-info btn-block my-4')); 
            echo form_close(); 
         ?> 
         </div>
         <h3>RANDRIANEKENA Fitiavana Niavo ETU000838 N-42 Promo11B</h3>         
         <h3>RAZAFINJATO Andriatefy Francky ETU000851 N-55 Promo11B</h3>
         <p>Admin:Niavo mdp:0000</p>         
         <p>Admin:Francky mdp:0000</p>         
         <p>Serveuse:Toky mdp:0000</p>         
      </div>
   
   <script type="text/javascript" src="<?php print(base_url());?>assets/js/jquery-3.4.1.min.js"></script>
   <script type="text/javascript" src="<?php print(base_url());?>assets/js/bootstrap.js"></script>
   <script type="text/javascript" src="<?php print(base_url());?>assets/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php print(base_url());?>assets/js/mdb.js"></script>
   <script type="text/javascript" src="<?php print(base_url());?>assets/js/mdb.min.js"></script>
   <script type="text/javascript" src="<?php print(base_url());?>assets/js/popper.min.js"></script>
   </body>
</html>