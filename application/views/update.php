<section class="ftco-section">
         <div class="container">
            <div class="row">
            <div class="col-md-12 heading-section text-center d-flex justify-content-around">
        <?php
            echo form_open('accueilController/update',array('class' => 'text-center border border-dark p-5'));           
            echo form_input(array('name'=>'id','value'=>$id,'type'=>'hidden'));
            echo form_label('Nom :');
            echo form_input(array('name'=>'nom'));
            echo form_label('Categorie :');
            echo form_input(array('name'=>'categorie'));
            echo form_label('prix :');
            echo form_input(array('name'=>'prix'));            
            echo form_submit(array('value'=>'Modifier','class' => 'btn btn-info btn-block my-4'));
            echo form_close();
            echo form_open('accueilController/delete',array('class' => 'text-center border border-dark p-5'));
            echo form_input(array('name'=>'id','value'=>$id,'type'=>'hidden'));
            echo form_submit(array('value'=>'Supprimer','class' => 'btn btn-info btn-block my-4'));
            echo form_close();
        ?>
</div>
               </div>
         </div>
      </section>