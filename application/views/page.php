<?php
   if(!isset($_SESSION['serveur'])){
      redirect(base_url().'index.php/indexController');
   }
   
?>

<!DOCTYPE html> 
<html lang = "en">
 
   <head> 
      <meta charset = "utf-8"> 
      <title>Login</title>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Nothing+You+Could+Do" rel="stylesheet">

      <link rel="stylesheet" href="<?php print(base_url());?>assets/css/open-iconic-bootstrap.min.css">
      <link rel="stylesheet" href="<?php print(base_url());?>assets/css/animate.css">
    
      <link rel="stylesheet" href="<?php print(base_url());?>assets/css/owl.carousel.min.css">
      <link rel="stylesheet" href="<?php print(base_url());?>assets/css/owl.theme.default.min.css">
      <link rel="stylesheet" href="<?php print(base_url());?>assets/css/magnific-popup.css">

      <link rel="stylesheet" href="<?php print(base_url());?>assets/css/aos.css">

      <link rel="stylesheet" href="<?php print(base_url());?>assets/css/ionicons.min.css">

      <link rel="stylesheet" href="<?php print(base_url());?>assets/css/bootstrap-datepicker.css">
      <link rel="stylesheet" href="<?php print(base_url());?>assets/css/jquery.timepicker.css">

    
      <link rel="stylesheet" href="<?php print(base_url());?>assets/css/flaticon.css">
      <link rel="stylesheet" href="<?php print(base_url());?>assets/css/icomoon.css">
      <link rel="stylesheet" href="<?php print(base_url());?>assets/css/style.css"> 
   </head> 
   <body> 
   <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="<?php echo base_url();?>index.php/indexController/Menu"><span class="flaticon-pizza-1 mr-1"></span>Resto<br><small>Antsika</small></a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">	          
	          <li class="nav-item active"><a href="<?php echo base_url();?>index.php/indexcontroller/Menu" class="nav-link">Menu</a></li>
	          <li class="nav-item"><a href="<?php echo base_url();?>index.php/commandeController" class="nav-link">Fin Commande</a></li>
	          <li class="nav-item"><a href="<?php echo base_url();?>index.php/indexController/Deconnexion" class="nav-link">Deconnexion</a></li>	          
	        </ul>
	      </div>
		  </div>
      </nav>
      
   <?php include($page.".php"); ?>

   <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

   <script src="<?php print(base_url());?>assets/js/jquery.min.js"></script>
  <script src="<?php print(base_url());?>assets/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?php print(base_url());?>assets/js/popper.min.js"></script>
  <script src="<?php print(base_url());?>assets/js/bootstrap.min.js"></script>
  <script src="<?php print(base_url());?>assets/js/jquery.easing.1.3.js"></script>
  <script src="<?php print(base_url());?>assets/js/jquery.waypoints.min.js"></script>
  <script src="<?php print(base_url());?>assets/js/jquery.stellar.min.js"></script>
  <script src="<?php print(base_url());?>assets/js/owl.carousel.min.js"></script>
  <script src="<?php print(base_url());?>assets/js/jquery.magnific-popup.min.js"></script>
  <script src="<?php print(base_url());?>assets/js/aos.js"></script>
  <script src="<?php print(base_url());?>assets/js/jquery.animateNumber.min.js"></script>
  <script src="<?php print(base_url());?>assets/js/bootstrap-datepicker.js"></script>
  <script src="<?php print(base_url());?>assets/js/jquery.timepicker.min.js"></script>
  <script src="<?php print(base_url());?>assets/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="<?php print(base_url());?>assets/js/google-map.js"></script>
  <script src="<?php print(base_url());?>assets/js/main.js"></script>
   </body>
</html>