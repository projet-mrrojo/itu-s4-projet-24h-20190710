      <section class="ftco-section">
         <div class="container">
            <div class="row">
            <div class="col-md-12 heading-section text-center d-flex justify-content-around">
               <?php
                  $commande=$_SESSION['commande'];
                  $ids="";
                  foreach($commande as $c){
                     for($i=0;$i<$c['nombre'];$i++){
                        $ids=$ids."".$c['id'].",";
                     }
                  }
                  $ids=substr($ids,0,-1);
                  $options=array();
                  foreach($result as $r){
                     $options[]=array($r->idTable=>$r->nom);
                  }
                  echo form_open('commandeController/set',array('class' => 'text-center border border-dark p-5'));
                  echo "<h5>".form_label('Table : ')."</h5>";
                  echo "<p>".form_dropdown('table',$options)."</p>";
                  echo "<p>".form_input(array('name'=>'plat','type'=>'hidden','value'=>$ids))."</p>";
                  echo "<p>".form_submit(array('value'=>'Commander','class' => 'btn btn-info btn-block my-4'))."</p>";
                  echo form_close();
               ?>
            </div>
               </div>
         </div>
      </section>