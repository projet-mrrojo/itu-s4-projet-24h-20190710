
   <section class="ftco-section">   
   <div class="container">
    	<div class="row justify-content-center mb-5 pb-3 mt-5 pt-5">
         <div class="col-md-7 heading-section text-center ftco-animate">
            <h2 class="mb-4">Notre Menu aujourd'hui:</h2>
            <p class="flip"><span class="deg1"></span><span class="deg2"></span><span class="deg3"></span></p>
         </div>
      </div>
      <div class="row">  
         <div class="col-md-12"> 
         <?php
               $i=1;
               foreach($list as $row){
         ?>
            <div class="pricing-entry d-flex ftco-animate">        			
        			   <div class="desc pl-3">
	        			   <div class="d-flex text align-items-center">
	        				   <h3><span><?php echo $row->nom; ?></span></h3>
                          <span class="price"><?php echo $row->prix; ?></span>
                          <button><a href="<?php echo base_url().'index.php/indexController/plus/'.$row->id; ?>">+</a></button>
	        			   </div>	        			   
	        		   </div>
        		</div>
            <?php                  
               $i = $i+1;
            }
            ?>
         </div>
      </div>
   </div>
   </section>