<section class="ftco-section">
         <div class="container">
            <div class="row">
            <div class="col-md-12 heading-section text-center d-flex justify-content-around">
         <?php
          $options=array();
          foreach($result as $r){
             $options[]=array($r->idTable=>$r->nom);
          }
            echo form_open('montantController/get',array('class' => 'text-center border border-dark p-5'));
            echo "<h5>".form_label('Avoir addition de la table : ')."</h5>";
            echo "<p>".form_dropdown('table',$options)."</p>";      
            echo "<p>".form_submit(array('value'=>'Montant A payer','class' => 'btn btn-info btn-block my-4'))."</p>";
            echo form_close();
         ?>
         </div>
               </div>
         </div>
      </section>