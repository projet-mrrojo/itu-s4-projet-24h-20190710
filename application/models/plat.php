<?php
    class Plat extends CI_Model {

        function __construct(){
            parent::__construct();
            $this->load->database();
        }

        public function getPlat(){
            $query = $this->db->get('plat');
            return $query;            
        }

        public function getSpecPlat($id){
            $sql="select * from plat where id=".$id;                      
            $query = $this->db->query($sql)->row();            
            return $query; 
        }

        public function insert($nom,$cat,$prix){
            $sql="insert into plat(nom,categorie,prix) values('".$nom."',".$cat.",".$prix.")";
            $this->db->query($sql); 
        }

        public function update($id,$nom,$cat,$prix){
            $attr=array("nom","categorie","prix");
            $str=array($id,$nom,$cat,$prix);
            $sql="update plat set";
            $v=0;
            for($i=0;$i<3;$i++){
                if($str[$i+1]==""){
                    $v++;
                }
                else{
                    $sql=$sql." ".$attr[$i]."='".$str[$i+1]."' , ";
                }
            }
            if($v==3){
                return;
            }
            $sql=substr($sql,0,-2);
            $sql=$sql." where id=".$id;
            $this->db->query($sql);
        }

        public function delete($id){
            $sql="delete from plat where id=".$id;
            $this->db->query($sql);
        }
        
        public function getToday(){
            $sql="select * from today";
            $id=$this->db->query($sql)->row();
            $ids=explode(',',$id->idPlats);            
            $result=array();
            for($i=0;$i<count($ids);$i++){
                $result[$i]=$this->getSpecPlat($ids[$i]);
            }
            return $result;
        }
    }
?>