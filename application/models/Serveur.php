<?php
    class Serveur extends CI_Model{
        
        function __construct(){
            parent::__construct();
            $this->load->database();
        }

        public function getServeur(){
            $this->db->select('*');                     
            $this->db->from('serveur');                     
            $this->db->where('profil =',1);                                 
            $query = $this->db->get()->row();            
            return $query; 
        }
        
        public function insertServeur($nom,$password){
            $sql="insert into plat(nom,motDePasse,profil) values('".$nom."',".$password.",1)";
            $this->db->query($sql);
        }

        public function updateServeur($id,$nom,$password,$profil){
            $attr=array("nom","motDePasse","profil");
            $str=array($id,$nom,$password,$profil);
            $sql="update serveur set";
            $v=0;
            for($i=0;$i<3;$i++){
                if($str[$i+1]==""){
                    $v++;
                }
                else{
                    $sql=$sql." ".$attr[$i]."='".$str[$i+1]."' , ";
                }
            }
            if($v==3){
                return;
            }
            $sql=substr($sql,0,-2);
            $sql=$sql." where id=".$id;
            $this->db->query($sql); 
        }

        public function deleteServeur($id){
            $sql="delete from serveur where id=".$id;
            $this->db->query($sql);
        }
    }
?>