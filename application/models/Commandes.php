<?php
    class Commandes extends CI_Model {

        function __construct(){
            parent::__construct();
            $this->load->database();
            $this->load->model('Plat');
        }

        public function getCommandes($tab){
            $this->db->select('*');                     
            $this->db->from('commandes');                     
            $this->db->where('idTable =',$tab);                                 
            $query = $this->db->get();            
            return $query;            
        }

        public function split($string){
            $result = explode(",",$string);
            return $result;
        }

        public function getMontant($idTable){
            $sql = "select idCommande,listPlat from commandes where idTable =".$idTable." and commandes.idCommande not in (select idCommande from payer)";            
            $temp=$this->db->query($sql);
            $result = $temp->row(0);
            $idPlats=$this->split($result->listPlat);
            $res=array();
            $tab = array();
            for($i=0;$i<count($idPlats);$i++){
                $tab[$i] = $this->Plat->getSpecPlat($idPlats[$i]);
            }
            $res[0]=$tab;
            $res[1]=$result->idCommande;
            return $res;
        }
    }
?>