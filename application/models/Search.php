<?php
    class Search extends CI_Model{

        function __construct(){
            parent::__construct();
            $this->load->helper('url');
            $this->load->database();
            $this->load->helper('form');
        }        

        public function searchPlat($string){
            $this->db->select('*');
            $this->db->from('plat');
            $this->db->like('nom',$string,'both');
            $query=$this->db->get()->result();
            return $query;
        }

        public function searchPlatCat($string,$string1){
            $this->db->select('*');
            $this->db->from('plat');
            $this->db->where('categorie',$string1);
            $this->db->like('nom',$string,'both');
            $query=$this->db->get()->result();
            return $query;
        }
        
    }
?>