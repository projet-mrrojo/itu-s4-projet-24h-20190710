create database projetRojo;
use projetRojo;

create table serveur(
    id int primary key auto_increment,
    nom varchar(50),
    motDePasse varchar(50),
    profil int
);

insert into serveur(nom,motDePasse,profil) values('Francky','0000',0);
insert into serveur(nom,motDePasse,profil) values('Niavo','0000',0);
insert into serveur(nom,motDePasse,profil) values('Toky','0000',1);

create table plat(
    id int primary key auto_increment,
    nom varchar(50),
    categorie int,
    prix int
);

create table categorie(
    id int primary key auto_increment,
    nom varchar(50)
);

alter table plat add foreign key(categorie) references categorie(id);

insert into categorie(nom) values('Plats');
insert into categorie(nom) values('Snack');
insert into categorie(nom) values('Boisson');

insert into plat(nom,categorie,prix) values('Henakisoa sy ravitoto',0,5000);
insert into plat(nom,categorie,prix) values('Assiette de Nems',1,4000);
insert into plat(nom,categorie,prix) values('Coca Cola',2,2000);
insert into plat(nom,categorie,prix) values('Tsaramaso sy saucisse',0,5000);
insert into plat(nom,categorie,prix) values('Canard laque',0,8000);
insert into plat(nom,categorie,prix) values('Brochettes',1,3000);
insert into plat(nom,categorie,prix) values('Fanta',2,2000);

create table tab(
    idTable int primary key auto_increment,
    nom varchar(3)
);

create table commandes(
    idCommande int primary key auto_increment,
    listPlat varchar(100),
    idTable int,    
    foreign key(idTable)REFERENCES tab(idTable)
);

insert into tab(nom) values('T1');
insert into tab(nom) values('T2');
insert into tab(nom) values('T3');
insert into tab(nom) values('T4');

insert into commandes(listPlat,idTable) values('1,1,1,2,3',1);

create table payer(
    idCommande int,
    montant int
);

create table parametre(
    designation varchar(50),
    valeur varchar(50)
);

insert into parametre values('nom','Resto-antsika');
insert into parametre values('Lieu','Andoharanofotsy');
insert into parametre values('slogan','Tsy votery mividy vao minana sakafo !!');
insert into parametre values('TVA','20');
insert into parametre values('nbEnre/page','4');

create table today(
    idPlats varchar(50)
);

insert into today values('1,2,3');